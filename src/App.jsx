import { useState } from 'react'
import reactLogo from './assets/react.svg'
import './App.css'

function App() {
  const [count, setCount] = useState(0)

  return (
    <div className="container">
      <div className="componente13">
        <div className="componente1">
          <img class="compo1" src="./src/assets/img/fut.png" alt="" />
          <h4 class="jugar">JUGAR FUTBOL</h4>
        </div>
        <div className="componente2">
          <img class="compo2" src="./src/assets/img/videojuegos.png" alt="" />
          <h4 class="video">JUGAR VIDEOJUEGOS</h4>
        </div>
        <div className="componente3">
          <img class="compo3" src="./src/assets/img/peliculas.png" alt="" />
          <h4 class="netflix">VER SERIES Y PELIS</h4>
        </div>
      </div>



      <div className="container2">
        <div className="componente4">
          <img class="compo4" src="./src/assets/img/box.png" alt="" />
          <h4 class="box">PRACTICAR BOX</h4>
        </div>
        <div className="componente5">
          <img class="compo5" src="./src/assets/img/musica.png" alt="" />
          <h4 class="musica">ESCUCHAR MUSICA</h4>
        </div>
      
      </div>
      
    </div>
    
    
  )
}

export default App
